from __future__ import annotations
import sys
import curses
from curses import wrapper

from gozer_curses.window import (
    BrowserWindow,
    Canvas,
    AddressBar
)
from gozer_curses.interface import Interface

from gozer_curses.manager import WindowManager

def main(stdscr: Window):
    '''Main event loop of the browser
    :param stdscr: Curses main window object
    :type stdscr: curses.Window
    '''
    stdscr.refresh()

    browser_window = BrowserWindow(stdscr)
    canvas = Canvas()
    address_bar = AddressBar()
    interface = Interface()

    window_manager = WindowManager(
        browser_window,
        canvas,
        address_bar,
        interface
    )

    if len(sys.argv) > 1 and sys.argv[1]:
        window_manager.start(uri=sys.argv[1])
    else:
        window_manager.start(splash=True)

    while True:
        stdscr.refresh()
        c = stdscr.getch()
        # TODO: Perhaps make this even more
        # generic (e.g. interaction_received)
        window_manager.send_key_press(c)

def init_app():
    wrapper(main)