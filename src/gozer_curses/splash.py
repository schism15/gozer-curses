from random import randint

banner_text_standard = (
'  ____  ___ __________ ____   ',
' / ___|/ _ \__  / ____|  _ \  ',
'| |  _| | | |/ /|  _| | |_) | ',
'| |_| | |_| / /_| |___|  _ <  ',
' \____|\___/____|_____|_| \_\\'
)

banner_text_shadow = (
'  ___|  _ \__  / ____|  _ \  ',
' |     |   |  /  __|   |   | ',
' |   | |   | /   |     __ <  ',
'\____|\___/____|_____|_| \_\ '
)

banner_text_slant = (
'   __________  _____   __________ ',
'  / ____/ __ \/__  /  / ____/ __ \\',
' / / __/ / / /  / /  / __/ / /_/ /',
'/ /_/ / /_/ /  / /__/ /___/ _, _/ ',
'\____/\____/  /____/_____/_/ |_|  '
)

# TODO: Maybe a list?
font_map = {
    0: banner_text_standard,
    1: banner_text_shadow,
    2: banner_text_slant
}

def banner_selector(font_map: dict) -> tuple:
    '''Randomly select banner text.

    :param font_map: Available banner text
    :type font_map: dict
    :return: Randomly selected banner text
    :rtype: tuple
    '''
    key = randint(0,2)
    return font_map[key]

welcome_text = (
    'Welcome to Gozer, a terminal based Gopher browser.'
)

divider = '**************************************************'

help_text = (
    'g - Edit Address Bar',
    'Enter - Submit address input into address bar or select highlighted link',
    'Page up - Move up one page',
    'Page down - Move down one page',
    'Left arrow - Go back one page in history',
    'Right arrow - Go forward one page in history',
    'Tab - Jump to the next link',
    'Shift-Tab - Jump to the previous link'
)

addtl_text = (
    'Please report any issues at https://gitlab.com/schism15/-/issues',
    'Ver. 0.1.0-alpha'
)


def assemble_splash_page(width: int) -> str:
    '''Assemble the splash page text in a
    center justified format

    :param width: total width of the window
    :type width: int
    :return: splash page text
    :rtype: str
    '''
    splash_page = ''
    splash_page += (5*'\n')

    banner_text = banner_selector(font_map)

    for line in banner_text:
        splash_page += line.center(width) + '\n'

    splash_page += '\n'
    splash_page += welcome_text.center(width) + '\n'
    splash_page += '\n'
    splash_page += divider.center(width) + '\n'
    splash_page += '\n'

    for line in help_text:
        splash_page += line.center(width) + '\n'
    
    splash_page += (5*'\n')
    for line in addtl_text:
        splash_page += line.center(width) + '\n'

    return splash_page

def assemble_splash_page_agnostic() -> str:
    ''' Assemble the splash page text (frontend agnostic)

        :return: splash page text
        :rtype: str
    '''
    splash_page = ''
    splash_page += (5*'\n')

    banner_text = banner_selector(font_map)

    for line in banner_text:
        splash_page += line + '\n'

    splash_page += '\n'
    splash_page += welcome_text + '\n'
    splash_page += '\n'
    splash_page += divider + '\n'
    splash_page += '\n'

    for line in help_text:
        splash_page += line + '\n'
    
    splash_page += (5*'\n')
    for line in addtl_text:
        splash_page += line + '\n'

    return splash_page