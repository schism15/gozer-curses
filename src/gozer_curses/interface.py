from gozer_curses.base import BaseComponent

class Interface(BaseComponent):
    def key_handler(self, key: str):
        '''Send event messages to the mediator
        based on user key press

        :param key: Key value from main event loop
        :type key: int
        '''
        # Window should have a change focus
        # (or at least for now a
        # focus-on-text-box) method that this
        # key event will trigger
        if key == ord('g'):
            # Send notification with 'g'
            self.mediator.notify(self, {
                'msg': 'editor_focused_address'
            })
        elif key == ord('q'):
            self.mediator.notify(self, {
                'msg': 'exit_program',
                'data': {
                    'exit_code': 0,
                },
            })
        elif key == ord('r'):
            self.mediator.notify(self, {
                'msg': 'reload_page'
            })
        elif key == 10:
            self.mediator.notify(self, {
                'msg': 'link_selected'
            })
        else:
            page_shift_keys = [
                338, # curses.KEY_PPAGE,
                339  # curses.KEY_NPAGE
            ]

            link_shift_keys = [
                353, # Shift+Tab
                9    # Tab
            ]

            history_shift_keys = [
                261,  # curses.KEY_RIGHT
                260   # curses.KEY_LEFT
            ]

            if key in page_shift_keys:
                self.mediator.notify(self, {
                    'msg': 'page_shift_key_press_received',
                    'data': {
                        'key': key,
                    },
                })
            elif key in link_shift_keys:
                self.mediator.notify(self, {
                    'msg': 'link_shift_key_press_received',
                    'data': {
                        'key': key,
                    },
                })
            elif key in history_shift_keys:
                self.mediator.notify(self, {
                    'msg': 'history_shift_key_press_received',
                    'data': {
                        'key': key,
                    }
                })
            else:
                pass
