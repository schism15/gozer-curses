from __future__ import annotations
import time
import sys
from abc import ABC

from gozer_curses.base import BaseMediator

from gozer_engine.gozer import Gozer

class WindowManager(BaseMediator):
    '''Central mediator object of the browser. Handles communication
       between all browser components
    '''
    def __init__(self, window: BrowserWindow, canvas: Canvas,
            address_bar: AddressBar, interface: Interface) -> None:
        self._window = window
        self._window.mediator = self
        self._canvas = canvas
        self._canvas.mediator = self
        self._address_bar = address_bar
        self._address_bar.mediator = self
        self._interface = interface
        self._interface.mediator = self

        # TODO: Better naming, re-evaluate location
        self._tree = None
        self._page_str = ''
        self._request_type = None
        self._dom = []
        # TODO: This gets passed in with window_manager_initiated,
        # but does not get used until window_initialized. Find
        # better location
        self._splash = None

        self._session = Gozer()

    def start(self, uri: str = None, splash: bool = False):
        '''
        '''
        self.submit_initial_uri(uri)
        self._splash = splash
        self.notify(self, {
            'msg': 'window_manager_initiated',
            'data': {
                'uri': uri
            }
        })

    def submit_initial_uri(self, uri):
        self._session.start(uri=uri)
        self._page_str = self._session.get_page_contents()

    # TODO: Better naming
    def send_key_press(self, key: str):
        '''Sends key value captured from main event loop
        to the mediator.

        :param key: Curses character value
        :type key: int
        '''
        self.notify(self, {
            'msg': 'key_press_received',
            'data': {
                'key': key,
            }
        })

    def notify(self, sender: object, event: dict) -> None:
        '''Receive events from other components and take
        action based on message/data

        :param sender: Component object sending the event
        :type object:
        :param event: Payload from Component object containing message and data
        :type event: dict
        '''
        # Initialization events
        if event['msg'] == 'window_manager_initiated':
            self._window.init_window()
        if event['msg'] == 'window_initialized':
            container = event['data']['container']
            if self._splash is True:
                self._page_str = ''
                self._canvas.init_canvas(container, splash=True)
            else:
                self._canvas.init_canvas(container)
                self._canvas._paint(
                    self._page_str,
                    back_to_top=True
                )
            self._address_bar.init_bar(container)

        # Network request events
        if event['msg'] == 'uri_received':
            self._request_type = event['data']['gopher_type']
            
            self._session.send_request(
                event['data']['uri'],
                request_type=self._request_type
            )
            self._page_str = self._session.get_page_contents()

            self._canvas._paint(
                self._page_str,
                back_to_top=True,
                request_type=self._request_type
            )

        # Interface events - General
        if event['msg'] == 'editor_focused_address':
            self._address_bar.listen()
        if event['msg'] == 'editor_focused_search':
            self._address_bar.listen(
                search=True,
                active_link=event['data']['active_link']
            )
        if event['msg'] == 'key_press_received':
            self._interface.key_handler(event['data']['key'])
        if event['msg'] == 'reload_page':
            self._canvas._paint(
                self._page_str,
                back_to_top=True
            )
        if event['msg'] == 'exit_program':
            sys.exit("Exiting Gozer Browser...")
        if event['msg'] == 'link_selected':
            self._canvas.gather_active_link()

        # Interface events - Page movement
        if event['msg'] == 'page_shift_key_press_received':
            self._canvas.shift_page_position(
                event['data']['key'],
                self._page_str
            )
        if event['msg'] == 'link_shift_key_press_received':
            self._canvas.shift_link_highlight(
                event['data']['key'],
                self._session.get_page_dom()
            )
        if event['msg'] == 'dom_updated':
            self._dom = event['data']['updated_dom']

        # Interface events - History movement
        if event['msg'] == 'history_shift_key_press_received':
            if event['data']['key'] == 260:
                # Back one page. Do not try to preserve
                # the vertical position on previous pages
                self._session.back_one_page()
                self._canvas._paint(
                    self._session.get_page_contents(),
                    back_to_top=True
                )
            elif event['data']['key'] == 261:
                # Forward one page. Do not try to preserve
                # the vertical position on previous pages
                self._session.forward_one_page()
                self._canvas._paint(
                    self._session.get_page_contents(),
                    back_to_top=True
                )

