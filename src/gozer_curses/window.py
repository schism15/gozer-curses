from __future__ import annotations
import curses
from curses import wrapper
from curses.textpad import Textbox, rectangle

from gozer_curses.base import BaseComponent
from gozer_curses.splash import assemble_splash_page

class BrowserWindow(BaseComponent):
    def __init__(self, stdscr: Window):
        self._stdscr = stdscr
        super().__init__(self)

    def init_window(self, splash: bool = False):
        '''Initialize main browser window

        :param splash: Canvas should display splash page
        :type splash: bool
        '''
        self._width = curses.COLS
        self._height = curses.LINES
        self._h_padding = 1
        self._v_padding = 1

        event_data = {
            'container': {
                'width': self._width,
                'height': self._height,
                'h_padding': self._h_padding,
                'v_padding': self._v_padding,
            }
        }

        # if splash:
        #     event_data['splash'] = True
        # else:
        #     event_data['splash'] = False

        self.mediator.notify(self, {
            'msg': 'window_initialized',
            'data': event_data
        })


class Canvas(BaseComponent):
    def init_canvas(self, container: dict, splash: bool = False):  # TODO: Better naming
        '''Initialize can

        :param splash: Display splash page template after canvas init
        :type splash: bool
        '''
        self._width = container['width'] - (2 * container['h_padding'])
        self._height = container['height'] - (4 * container['v_padding'])
        self._begin_x = container['h_padding']
        self._begin_y = container['v_padding']

        self.canvas = curses.newwin(
            self._height,
            self._width,
            self._begin_y,
            self._begin_x
        )

        self._printable = curses.newwin(  # TODO: Better naming
            self._height - 2,
            self._width - 1,
            self._begin_y + 1,
            self._begin_x + 1
        )

        # TODO: This is less about the stucture of
        # the browser window and more about keeping
        # track of scroll position in the page.
        # Review where this attribute should be located.
        self._current_line = 0
        # TODO: Will likely move to mediator
        self._link_map = []
        self._dom = []
        # self._page_str = ''

        # TEMP: For tracking, the 'visible' portion of the DOM.
        # When tabbing through the links, if the next link is
        # outside the visible window, send a 'page down' signal
        # to the Mediator
        self._printable_begin = self._begin_y + 1
        self._printable_end = (self._begin_y + 1) + (self._height - 4)

        if splash:
            splash_page_str = assemble_splash_page(self._width - 2)
            try:
                self._printable.addstr(splash_page_str)
            except curses.error:
                pass

        self.canvas.border()
        self.canvas.refresh()
        self._printable.refresh()

    # START TODO ################################
    # Refactor to reduce redundancy  
    def _page_down(self, page_str: str):
        '''Move visible canvas one page down document

        :param page_str: Gopher page source code
        :type page_str: str
        '''
        # Prevent out of bounds movement
        parsed = self._parse_lines(page_str)
        scr_buf = parsed[self._current_line:]
        if len(scr_buf) > self._height - 2:
            self._current_line = self._current_line + (self._height - 2)
            self._paint(page_str)

    def _page_up(self, page_str: str):
        '''Move visible canvas one page up document

        :param page_str: Gopher page source code
        :type page_str: str
        '''
        # Prevent out of bounds movement
        new_line_no = self._current_line - (self._height - 2)
        if new_line_no >= 0:
            self._current_line = new_line_no
            self._paint(page_str)
    # END TODO ###########################################


    # TODO START ################################
    #  Move to browser or render engine
    # as appropriate
    def _parse_lines(self, page_str: str) -> list:
        '''Split page source by newline character

        :param page_str: Gopher page source code
        :type page_str: str
        :return: list of page source lines
        :rtype: list
        '''
        return page_str.split('\n')
    # TODO END ##################################

    def shift_page_position(self, key: int, page_str: str):
        '''Trigger page movement function base on user input

        :param key: User input from main event loop
        :type key: int
        :param page_str: Gopher page source code
        :type page_str: str
        '''
        if key == curses.KEY_PPAGE:
            self._page_up(page_str)
        if key == curses.KEY_NPAGE:
            self._page_down(page_str)

    def shift_line_position(self, key: int, page_str: str):
        '''Trigger line movement function base on user input

        :param key: User input from main event loop
        :type key: int
        :param page_str: Gopher page source code
        :type page_str: str
        '''
        if key == curses.KEY_UP:
            self._line_up(page_str)
        if key == curses.KEY_DOWN:
            self._line_down(page_str)    

    def _refresh_link_map(self, dom: list = None):
        '''Recreate link map based on current DOM

        :param dom: Document Object Model
        :type dom: list
        '''
        if not dom:
            dom = self._dom
        link_types = [ 0, 1, 7]

        link_map = []

        for idx, node in enumerate(dom):
            if node['data'].get('gopher_type') in link_types:
                link_map.append(node)
        
        self._link_map = link_map

    def shift_link_highlight(self, key: int, dom: list):
        '''Trigger function to change active link

        :param key: User input from main event loop
        :type key: int
        :param dom: Document Object Model
        :type page_str: list
        '''
        # TODO: Should be cast to str during
        # during rendering
        link_types = [ 0, 1]
        
        self._dom = dom
        self._refresh_link_map(self._dom)

        if key == 353:
            self._prev_link()
        if key == 9:
            self._next_link()

    def _get_active_link(self) -> dict:
        '''Get the link marked active in the DOM

        :return: active link
        :rtype: dict
        '''
        result = (-1, None)
        for idx, link in enumerate(self._link_map):
            if link['active'] is True:
                result = (idx, link)

        return result

    def _set_link_active_state(self, link_map_idx: int, active: bool = False):
        '''Get the active state value for a given link

        :param link_map_idx: position of the link in the link_map
        :type link_map_idx: int
        :param active: Is link active or not
        :type active: bool
        '''
        # TODO: Pick variable names that will clearly differentiate
        # the position in the DOM from the position in the link_map
        dom_idx = self._link_map[link_map_idx]['position']
        if active is True:
            self._link_map[link_map_idx]['active'] = True
            self._dom[dom_idx]['active'] = True
        elif active is False:
            self._link_map[link_map_idx]['active'] = False
            self._dom[dom_idx]['active'] = False

    # TODO: Refactor to pull the "swap active link" logic out of
    # _prev_link and _next_link into a separate util function that
    # both can use. Do the same for the "save change to dom" functionality
    # TODO: Ensure a coherent separation/relationship among the following:
    # - link highlight switch (visual)
    # - link focus switch (semantic)
    # TODO: Refactor _prev_link and _next_link for overall readability
    def _prev_link(self):
        '''Switch active status to the next link in the link_map and
        highlight it on the canvas
        '''
        # An index of negative one means, no link
        # currently active
        current_pos, current_active = self._get_active_link()


        if current_active is not None and current_pos != -1:
            # Position of the current link, relative to the visible page
            relative_current_position = self._get_relative_position(current_active)
            
            if current_pos > 0:
                target_pos, target_active = (current_pos - 1, self._link_map[current_pos - 1])

                # Position of the target link, relative to the visible page
                relative_target_position = self._get_relative_position(target_active)

                self._set_link_active_state(current_pos, active=False)
                self._set_highlight_state(relative_current_position, state='off')
                self._refresh_link_map()
                self._set_link_active_state(target_pos, active=True)
                
                if target_active['position'] < self._printable_begin:
                    self._page_up(self._page_str)
                    self._printable.refresh()

                    self._set_highlight_state(relative_target_position, state='on')
                    self._printable.refresh()

                    self._printable_begin = target_active['position'] - relative_target_position
                    self._printable_end = self._printable_begin + (self._height - 4)
                else:
                    self._set_highlight_state(relative_target_position, state='on')
                    self._printable.refresh()
            else:
                target_pos, target_active = (0, self._link_map[0])
                self._set_link_active_state(target_pos, active=True)
                self._set_highlight_state(relative_target_position, state='on')
        else:
            target_pos, target_active = (0, self._link_map[0])
            self._set_link_active_state(target_pos, active=True)
            self._set_highlight_state(target_active['position'], state='on')

        self.canvas.border()
        self._printable.refresh()

        self.mediator.notify(self, {
            'msg': 'dom_updated',
            'data': {
                'updated_dom': self._dom,
            }
        })

    def _next_link(self):
        '''Switch active status to the next link in the link_map and
        highlight it on the canvas
        '''
        
        # The contents and position of the currently
        # highlighted link.
        current_pos, current_active = self._get_active_link()

        
        # An index of negative one means, no link
        # currently active
        if current_active is not None and current_pos != -1:
            # Position of the current link, relative to the visible page
            relative_current_position = self._get_relative_position(current_active)
            
            if current_pos < len(self._link_map) - 1:
                # Active link falls within the range of the link map
                target_pos, target_active = (current_pos + 1, self._link_map[current_pos + 1])

                # Position of the target link, relative to the visible page
                relative_target_position = self._get_relative_position(target_active)

                # Deactivate/un-highlight the current active link,
                # then activate the target link
                self._set_link_active_state(current_pos, active=False)
                self._set_highlight_state(relative_current_position, state='off')
                self._refresh_link_map()
                self._set_link_active_state(target_pos, active=True)


                # If the target link is on the next page of text
                # (i.e. not currently visible in the canvas window),
                # first page down, the highlight the line for the
                # target link.
                if target_active['position'] > self._printable_end:
                    self._page_down(self._page_str)
                    self._printable.refresh()

                    self._set_highlight_state(relative_target_position, state='on')
                    self._printable.refresh()
                    
                    self._printable_begin = target_active['position'] - relative_target_position
                    self._printable_end = self._printable_begin + (self._height - 4)
                else:
                    self._set_highlight_state(relative_target_position, state='on')
                    self._printable.refresh()

            else:
                target_pos, target_active = (len(self._link_map) - 1, self._link_map[len(self._link_map) - 1])
                self._set_link_active_state(target_pos, active=True)
                self._set_highlight_state(relative_target_position, state='on')
        else:
                target_pos, target_active = (0, self._link_map[0])
                self._set_link_active_state(target_pos, active=True)
                self._set_highlight_state(target_active['position'], state='on')

        self.canvas.border()
        self._printable.refresh()

        self.mediator.notify(self, {
            'msg': 'dom_updated',
            'data': {
                'updated_dom': self._dom,
            }
        })
    
    def _get_relative_position(self, link):
        return link['position'] % (self._height - 2)  # self._printable height

    def _set_highlight_state(self, canvas_line_no: int, state: str = 'off'):
        '''Update Curses line attribute to set/unset highlight

        :param canvas_line_no: Line on the visible canvas
        :type canvas_line_no: int
        :param state: Highlighted of not highlighted
        :type state: bool
        '''
        if state == 'on':
            attr = curses.A_REVERSE
        elif state == 'off':
            attr = curses.A_HORIZONTAL

        self._printable.chgat(
            canvas_line_no,
            1,
            self._width,
            attr
        )

    def gather_active_link(self):
        '''Send the current active link in the DOM to the mediator'''
        active_pos, active_link = self._get_active_link()
        if active_link is not None and active_pos != -1:
            if active_link['data']['gopher_type'] == 7:
                self.mediator.notify(self, {
                    'msg': 'editor_focused_search',
                    'data': {
                        'active_link': active_link
                    }
                })
            else:
                selector = active_link["data"]["selector"]
                if selector[0] != '/':
                    selector = f'/{selector}'
                uri = f'{active_link["data"]["host"]}{selector}'
                self.mediator.notify(self, {
                    'msg': 'uri_received',
                    'data': {
                        'uri': uri,
                        'gopher_type': active_link["data"]["gopher_type"]
                    }
                })
        else:
            pass

    def _paint(self, page_str: str, back_to_top: bool = False, request_type=1):
        '''Print the visible portion of the page to the Canvas
        window based on the size of the Canvas and the current
        line position.

        :param page_str: Gopher page source code
        :type page_str: str
        :param back_to_top: reset line position to 0, then paint
        :type back_to_top: bool
        '''
        self._page_str = page_str
        self._printable.clear()
        
        if back_to_top:
            self._current_line = 0

        parsed = self._parse_lines(page_str)
        page = parsed[self._current_line:(self._current_line + self._height - 2)]
        
        if request_type == 0:
            delimiter = '\n  '
        else:
            delimiter = '\n'
        
        # The addstr() methods print the text and advance
        # one line. There are no methods available in Python
        # curses that do not do this, so catch and ignore error
        try:
            self._printable.addstr(delimiter.join(page))
        except curses.error:
            pass

        self.canvas.border()
        self._printable.refresh()


class AddressBar(BaseComponent):
    def init_bar(self, container: dict):
        '''Create address bar based on parent window dimensions.
        
        :param container: Parent window dimensions sent from the mediator
        :type container: dict
        '''
        self._height = 3
        self._width = container['width'] - (2 * container['h_padding'])
        self._begin_y = (container['height'] - (4 * container['v_padding']))
        self._begin_x = 1

        self._label = curses.newwin(
            self._height,
            7,
            self._begin_y + 2,
            self._begin_x + 1
        )
        self._label.addstr('Go To')
        self._label.refresh()

        self._bar = curses.newwin(
            self._height,
            self._width - 7,  # Make room for the label
            self._begin_y + 1,
            self._begin_x + 7
        )

        self._editable = Textbox(curses.newwin(
            1,
            self._width - 8,
            self._begin_y + 2,
            self._begin_x + 8
        ))
        self._bar.border()
        self._bar.refresh()

    # TODO: May need to be moved to TBD
    # interaction Component
    def _submit_on_enter(self, char: int) -> int:
        '''If user input char is Enter (10), return curses.Textpad.edit()
        termination character. Otherwise return user input character

        :param char: User input character
        :type char: int
        :return: Either termination character or user input character
        :rtype: int
        '''
        if char == 10:
            return 7
        else:
            return char
    
    def listen(self, search: bool = False, active_link: dict = None):
        '''Accepts input from user for either a URI or a search query from.
        Sends URI to the mediator
        
        :param search: Indicate if this is a URI network request or a Gopher search
        :type search: bool
        :param active_link: DOM representation of a link
        :type active_link: dict
        '''
        if search is True:
            self._editable.do_command(1)  # CTRL A
            self._editable.do_command(11)  # CTRL K
            self._label.clear()
            self._label.addstr('Search')
            self._label.refresh()
            self._bar.border()
            self._bar.refresh()
        else:
            self._editable.do_command(1)  # CTRL A
            self._editable.do_command(11)  # CTRL K
            self._label.clear()
            self._label.addstr('Go To')
            self._label.refresh()
            self._bar.border()
            self._bar.refresh()

        self._editable.edit(self._submit_on_enter)
        data = self._read()

        if search is True:
            query = data
            host = active_link["data"]["host"]
            selector = f'/{active_link["data"]["selector"]}'
            uri = f'{host}{selector}\t{query}'
        else:
            uri = data

        self.mediator.notify(self, {
            'msg': 'uri_received',
            'data': {
                'uri': uri,
                'gopher_type': 1
            }
        })

    # TODO: Read/Write as methods name may
    # be too ambiguous. Reassess.
    def _write(self):
        '''Not implemented'''
        pass

    def _read(self) -> dict:
        '''Read address bar contents into a string'''
        # TODO: strip() is a workaround. Get a better
        # understanding of where the trailing whitespace
        # is coming from and handle better
        return self._editable.gather().strip()