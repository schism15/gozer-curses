from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name = "gozer_curses",
    version = "0.1.3",
    author = "schism",
    author_email = "schism@schism15.com",
    description = "A terminal browser engine for Gopher",
    long_description=long_description,
    license = "GPL v2",
    keywords = "gopher browser curses tui",
    url = "https://gitlab.com/schism15/gozer-curses",
    packages=find_packages('src', 'src.gozer_curses'),
    package_dir = {'': 'src'},
    entry_points= {
        'console_scripts': ['gozer=gozer_curses.main:init_app'],
    },
    install_requires = ['gozer-engine'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
        "Operating System :: POSIX :: Linux"
    ],
    python_requires='>=3.8',
)